﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateSpawnPoints : MonoBehaviour, ISetRandomSpawns
{
    public GameObject[] barrierPrefabs;
    public Vector3 SpawnPoint;
    public GameObject[] theseSpawnPoints;
    public float spawnPointDistance;
    public int boxType;  //static or pushback box type


    private void Start()
    {
        SpawnPoint = new Vector3(transform.position.x-4, transform.position.y, transform.position.z);
        Instantiation();
    }

    public bool InstantiationCheck()
    {
        int willISpawn = Random.Range(0, 2);
        if (willISpawn == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void Instantiation()
    {
        GameObject clone;
        int numberOfNulls = 0;
        for (int i = 0; i < theseSpawnPoints.Length; i++)
        {
            int barrierID = FindBarrierID();
            
            if (InstantiationCheck())
            {
                clone = Instantiate(barrierPrefabs[barrierID], SpawnPoint, Quaternion.identity) as GameObject;
                theseSpawnPoints[i] = clone.gameObject;
                clone.transform.position = SpawnPoint;
            }
            else
            {
                numberOfNulls++;
            }
            if (theseSpawnPoints[0] && theseSpawnPoints[1] && theseSpawnPoints[2] != null)
            {
                Destroy(theseSpawnPoints[Random.Range(0, 2)]);
            }
            if (theseSpawnPoints[3] && theseSpawnPoints[4] && theseSpawnPoints[5] != null)
            {
                Destroy(theseSpawnPoints[Random.Range(3, 5)]);
            }
            SpawnPoint.x = SpawnPoint.x + spawnPointDistance;
            boxType = Random.Range(1, 3);
            Debug.Log(boxType);
        }
        if (numberOfNulls > 5)
        {
            int barrierID = FindBarrierID();
            Vector3 emergencySpawn = SpawnPoint = new Vector3(transform.position.x , transform.position.y, transform.position.z);
            Vector3 emergencySpawnTwo = SpawnPoint = new Vector3(transform.position.x + 9, transform.position.y, transform.position.z);
            Instantiate(barrierPrefabs[barrierID], emergencySpawn, Quaternion.identity);
            Instantiate(barrierPrefabs[barrierID], emergencySpawnTwo, Quaternion.identity);
        }


    }
    public int FindBarrierID()
    {
        int thisBarrier = Random.Range(0, barrierPrefabs.Length);
        return thisBarrier;
    }

}
