﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateSpawnTerminals : MonoBehaviour, ISetRandomSpawns
{
    public GameObject spawnTerminalPrefab;
    public Vector3 terminalPoint;
    public float levelTimeLimit;
    public float cameraSpeed;
    public float terminalDistance;
    public float findTerminals;
    public int numberOfTerminals;
    public GameObject[] terminals;
    // Start is called before the first frame update
    void Start()
    {
        findTerminals = (levelTimeLimit * cameraSpeed) / terminalDistance;
        numberOfTerminals = Mathf.RoundToInt(findTerminals);
        terminals = new GameObject[numberOfTerminals];
        Instantiation();
    }

    public void Instantiation()
    {
        GameObject clone;
        for (int i = 0; i < numberOfTerminals; i++)
        {
                clone = Instantiate(spawnTerminalPrefab, terminalPoint, Quaternion.identity) as GameObject;
                terminals[i] = clone.gameObject;
                clone.transform.position = terminalPoint;

            terminalPoint.y = terminalPoint.y + terminalDistance;
        }
    }
}
