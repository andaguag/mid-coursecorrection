﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeBarrierShape : MonoBehaviour
{
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
    public int rotationMin;
    public int rotationMax;
    public float rotMultiplier;

    //Randomizes the shape and angle of the barriers on awake.
    void Awake()
    {
        //Randomize the length and width of each barrier that gets instantiated, between minX/minY and maxX/maxY.
        transform.localScale = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 1);
        //Randomize the angle of the barriers, between rotationMin and rotationMax
        transform.eulerAngles = new Vector3(0, 0, (Random.Range(rotationMin, rotationMax) * rotMultiplier));
    }
}


