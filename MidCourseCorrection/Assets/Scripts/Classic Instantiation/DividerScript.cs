﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DividerScript : MonoBehaviour
{
    public float levelLength;
    public float cameraSpeed;
    // Adjusts the size of the divider based on how long the level is.
public void Start()
    { 
        transform.localScale = new Vector3(transform.localScale.x, (levelLength * 2) +10, transform.localScale.z);
    }
}
