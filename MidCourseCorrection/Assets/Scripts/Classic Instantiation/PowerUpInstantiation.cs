﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpInstantiation : MonoBehaviour, ISetRandomSpawns
{

    public GameObject[] powerUps;

    public GameObject[] totalPowerUps;
    // Start is called before the first frame update
    void Start()
    {
        Instantiation();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Instantiation()
    {
        GameObject clone;
        int numberOfPowerUps = Random.Range(1,11);
        totalPowerUps = new GameObject[numberOfPowerUps];
        float objectX;
        float objectY;
        Vector3 powerUpCoordinates;
        int powerUpID;
        for (int i= 0; i < numberOfPowerUps; i++)
        {
            powerUpID = FindPowerUpID();
            objectX = Random.Range(-4, 4);
            objectY = Random.Range(0, (GameManager.levelLength * GameManager.cameraSpeed));
            powerUpCoordinates = new Vector3(objectX, objectY, transform.position.z);

            clone = Instantiate(powerUps[powerUpID], powerUpCoordinates, Quaternion.identity);
            totalPowerUps[i] = clone.gameObject;
            clone.transform.position = powerUpCoordinates;

        }
    }
    public int FindPowerUpID()
    {
        int thisPowerUp = Random.Range(0, powerUps.Length);
        return thisPowerUp;
    }
}
