﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPosition : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9 || collision. gameObject.layer == 8)
        {
            print("position reset");
            float objectX = Random.Range(-4, 4);
            float objectY = Random.Range(0, (GameManager.levelLength * GameManager.cameraSpeed));
            transform.position = new Vector3(objectX, objectY, transform.position.z);
        }
    }
}
