﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseScore : MonoBehaviour
{
    public ScoreManager scoreManager;
    public int scoreAddValue;
    public int playerLayer;
    public GameObject collectParticle;
    private void OnEnable()
    {
        scoreManager = FindObjectOfType<ScoreManager>();
    }
    //When the player runs into the powerup that increases the player's score, play a sound, add scoreAddValue to the player's score, and destroy the powerup.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == playerLayer)
        {
            Instantiate(collectParticle, transform.position, transform.rotation);
            scoreManager.AddScore(scoreAddValue);
            Destroy(gameObject);
        }
    }
}
