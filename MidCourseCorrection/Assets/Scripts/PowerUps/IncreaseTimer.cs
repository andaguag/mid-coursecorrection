﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseTimer : MonoBehaviour
{
    public TimeCount timer;
    public int timerAddValue;
    public int playerLayer;
    public GameObject collectParticle;
    private void OnEnable()
    {
        timer = FindObjectOfType<TimeCount>();
    }
    //When the player runs into the powerup that increases the timer, play a sound, add timerAddValue to the timer, and destroy the powerup.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == playerLayer)
        {
            Instantiate(collectParticle, transform.position, transform.rotation);
            timer.TimerAdd(timerAddValue);
            Destroy(gameObject);
        }
    }
}
