﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    public float cameraSpeed;
    public float levelTimer;
    public float levelLength;
    // Update is called once per frame
    void Update()
    {
        Timer();
        if (levelTimer < levelLength)
        Glide();
    }
    //A timer for the level, so that the camera doesn't keep moving when the level is over
    void Timer()
    {
        levelTimer = levelTimer + Time.deltaTime;
    }
    //Moves the camera by cameraSpeed every frame.
    void Glide()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + cameraSpeed * Time.deltaTime, transform.position.z);
    }
}
