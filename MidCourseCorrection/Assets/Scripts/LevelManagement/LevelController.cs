﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    public float cameraSpeed;
    public float levelLength;
    public MoveCamera mainCamera;
    public InstantiateSpawnTerminals spawnTerminal;
    public DividerScript dividerScript;


    private void Awake()
    {
        cameraSpeed = GameManager.cameraSpeed;
        levelLength = GameManager.levelLength;
        assignCameraValues();
        assignSpawnTerminalValues();
        assignDividerValues();
    }

    public void assignSpawnTerminalValues()
    {
            spawnTerminal = spawnTerminal.GetComponent<InstantiateSpawnTerminals>();
            spawnTerminal.cameraSpeed = cameraSpeed;
            spawnTerminal.levelTimeLimit = levelLength;
    }

    public void assignCameraValues()
    {
        mainCamera = mainCamera.GetComponent<MoveCamera>();
        mainCamera.cameraSpeed = cameraSpeed;
        mainCamera.levelLength = levelLength+5;
    }

    public void assignDividerValues()
    {
        dividerScript = dividerScript.GetComponent<DividerScript>();
        dividerScript.levelLength = levelLength;
        dividerScript.cameraSpeed = cameraSpeed;
    }
}
