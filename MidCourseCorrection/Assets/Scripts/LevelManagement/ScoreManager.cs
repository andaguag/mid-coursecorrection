﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public string scoreName;
    public Text scoreText;

    private void OnEnable()
    {
        updateScore();
    }
    //Add to the score whatever value is passed in, then update the score on-screen.
    public void AddScore(int scoreIncrease)
    {
        GameManager.score += scoreIncrease;
        updateScore();
    }

    public void updateScore()
    {
        scoreText.text = scoreName + GameManager.score;
    }
}
