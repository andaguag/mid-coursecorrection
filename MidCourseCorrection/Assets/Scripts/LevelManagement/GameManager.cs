﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(menuName = "LevelManager")]
public class GameManager: ScriptableObject
{
    public static float cameraSpeed = 1f;
    public static float levelLength = 30;
    public static float speedScale = 0.2f;
    public static float lengthScale = 1;
    public static float initialSpeed;
    public static float initialLength;
    public static bool levelLockBool = false;
    public static int score;
    public static int currentLevel = 1;
    public static int levelCompleteScoreBonus = 100;
    public static bool playerDead;

    //Stores the initial speed of the camera speed and the length of the level, so that when it gets reset it can return to these values.
    private void OnEnable()
    {
        initialSpeed = cameraSpeed;
        initialLength = levelLength;
        currentLevel = 1;
        score = 0;
    }

    //Loads the next level and increases the difficulty
    public static void BeatLevel()

    {
        if (!levelLockBool)
        {

            cameraSpeed = cameraSpeed + speedScale;
            levelLength = levelLength + lengthScale;
            currentLevel++;
            levelLockBool = true;
            score += levelCompleteScoreBonus;
            SceneManager.LoadScene(2);
        }
        
    }

    //Loads the Next Level screen
    public void NextLevel()
    {
        levelLockBool = false;
        SceneManager.LoadScene(1);
    }

    //Loads the Game Over screen
    public static void GameOver()
    {
        playerDead = false;
        SceneManager.LoadScene(3);
    }

    public static void ResetGame()
    {
        cameraSpeed = initialSpeed;
        levelLength = initialLength;
        currentLevel = 1;
        score = 0;
        SceneManager.LoadScene(1);
    }

}
