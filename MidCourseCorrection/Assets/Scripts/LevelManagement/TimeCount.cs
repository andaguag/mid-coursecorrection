﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCount : MonoBehaviour
{
    public string timerName;
    public float timerMax;
    public float timerMin;
    public Text timerText;
    public float currentTime;
    public float roundingValue;
    public bool countsDown;
    public bool minimumTimeReached;

    public string countdownA;
    public string countdownB;
    // Start is called before the first frame update
    void Start()
    {
        currentTime = timerMax;
        timerText = GetComponent<Text>();
        timerText.text = "Timer: " + currentTime;
    }

    void FixedUpdate()
    {
        if (!GameManager.playerDead)
        {
            //If the countsDown bool is set to false, the timer will increase up to a certain amount before it resets to 0.
            if (!countsDown)
            {
                Timer(1);
                timerText.text = timerName + currentTime;
                if (timerMax != 0 && currentTime > timerMax)
                {
                    currentTime = timerMin;
                }
            }
            //if the countsDown bool is set to true, the timer will start at a certain amount and decrease until it hits 0, at which time it will reset to the maximum amount again.
            if (countsDown)
            {
                Timer(-1);
                timerText.text = timerName + currentTime;
                if (currentTime <= 2 && currentTime > 1.5)
                {
                    //play a sound
                }
                if (currentTime <= 1 && currentTime > 0.5)
                {
                    //play a sound
                }
                if (currentTime <= 0)
                {
                    //play a sound
                }
                if (currentTime < timerMin)
                {
                    currentTime = timerMax;
                    minimumTimeReached = true;
                }
            }
        }
    }

    public void Timer(int PlorMi)
    {
        currentTime = currentTime + (Time.deltaTime * PlorMi);
        currentTime = Mathf.Round(currentTime * roundingValue) / roundingValue;
    }
    public void TimerAdd(float addTime)
    {
        currentTime = currentTime + addTime;
    }
}

