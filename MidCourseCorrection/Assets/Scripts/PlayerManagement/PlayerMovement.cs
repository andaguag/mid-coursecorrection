﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    Vector3 playerPosition;
    public Vector3 targetDestination;
    public float moveSpeed;// Start is called before the first frame update
    public bool RightSideActive;
    //public float timeLeft;
    //public Text timerDisplay;

    public SwitchControls switchControls;

    public GameObject background1;
    public GameObject background2;

    Rigidbody rb;

     void Start()
    {
        //base.Start();
        rb = GetComponent<Rigidbody>();
        targetDestination = transform.position;
        background1.SetActive(true);
        RightSideActive = false;
}

    void Update()
    {
        //CheckBool();
        CheckMousePos();
        movePlayer();
    }

    void movePlayer()
    {
        rb.MovePosition(Vector3.MoveTowards(rb.position, targetDestination, moveSpeed * Time.deltaTime));
    }

    //void CheckBool(bool right) {
    //    if (right == true)
    //    {
    //        RightSideActive = true;
    //    }
    //    else {
    //        RightSideActive = false;
    //    }
    //}

    public void SideSwitch(bool right)
    {  
        if (right == true)
        {
            RightSideActive = false;
            background1.SetActive(true);
            background2.SetActive(false);
            Debug.Log("left side");

        }
        else 
        {
            RightSideActive = true;
            background1.SetActive(false);
            background2.SetActive(true);
            Debug.Log("right side");
        }
    }

    void CheckMousePos()        //set direction to new inidcated position.
    {
        if (Input.GetMouseButtonDown(0))        //mouse left mouse down
        {
            Vector2 currentMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);      //current mouse position = mouse as on current screen

            if ((currentMousePos.x < 0 && !RightSideActive) || (currentMousePos.x > 0 && RightSideActive))        // if mouse position/x is greater than 0 and isBlue is true OR mouse position.x is less than 0 and bool is false
            {
                targetDestination = currentMousePos;

                if (Mathf.Sign(targetDestination.x) == Mathf.Sign(-transform.position.x))          //if destination is a negative x then reflect for other side
                {
                    targetDestination.x *= -1;
                }
            }
        }
    }
}
//the boolean isnt changeing for both scripts for some reason....?