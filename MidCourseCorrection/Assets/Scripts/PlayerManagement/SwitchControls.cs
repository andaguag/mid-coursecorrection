﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchControls : MonoBehaviour
{
    public bool right = true;
    public float timeLeft = 3;
    public Text timerDisplay;

    private PlayerMovement playerMove;

    public void timeCountDown() { 
    timerDisplay.text = timeLeft.ToString();
        timeLeft -= Time.deltaTime;
        if (timeLeft< 0)
        {
            if (right == true)
            {
                right = false;
            }
            else {
                right = true;
            }
            playerMove.SideSwitch(right);

            timeLeft = Random.Range(3, 6);
            Debug.Log("SWITCH");
        }
}
}
